const web3 = require('web3');
const web3_instance = new web3(new web3.providers.HttpProvider('http://127.0.0.1:4444'));
var contract_address = '0x03F23ae1917722d5A27a2Ea0Bcc98725a2a2a49a';
contract_address = web3_instance.utils.toChecksumAddress("0x03F23ae1917722d5A27a2Ea0Bcc98725a2a2a49a");

var Tx = require('ethereumjs-tx').Transaction;

const abi = require('./abi');
const stackeable_instance = new web3_instance.eth.Contract(abi, contract_address);


/*
Function to add an address to StakeHolding.

@params 
address String  

Example "0xDc29b9CE633F63394F1C5c4A6F9a6701F18fa9Ef"
*/


async function addStakeHolder(address) {
    try {
        let stakeholder = web3_instance.utils.toChecksumAddress(address);
        let contract_owner = web3_instance.utils.toChecksumAddress("0xcd2a3d9f938e13cd947ec05abc7fe734df8dd826")
        await stackeable_instance.methods.addStakeholder(stakeholder).send({ from: contract_owner }).then(function (tx) {
            console.log("Transaction: ", tx);
        });
    } catch (e) {
        throw new Error(e.message)
    } finally {
        return;

    }
}

/*
Function that returns Boolean whether is a StakeHolder or not.

@params
stakeHolder String

Example "0xDc29b9CE633F63394F1C5c4A6F9a6701F18fa9Ef"
*/


async function isStakeHolder(stakeHolder) {
    let address = web3_instance.utils.toChecksumAddress(stakeHolder);
    await stackeable_instance.methods.isStakeholder(address).call().then(function (tx) {
        let isStaker = JSON.parse(tx["0"])
        return cb(isStaker);
    });
}

/*
Function that returns total of staking tokens of an address.

@params
address String

Example "0xDc29b9CE633F63394F1C5c4A6F9a6701F18fa9Ef"
*/

async function getStakingTokens(address, cb) {
    address = web3_instance.utils.toChecksumAddress(address)
    try {
        await stackeable_instance.methods.stakeOf(address).call().then(function (totalStakedTokens) {

            if (totalStakedTokens) {
                return cb(parseInt(totalStakedTokens))
            }

        })
    } catch (e) {
        throw new Error(e.message)
    } finally {
        return;

    }
}

/*
Function that returns total of rewards of an address.

@params
address String

Example "0xDc29b9CE633F63394F1C5c4A6F9a6701F18fa9Ef"
*/

async function getRewards(address, cb) {
    address = web3_instance.utils.toChecksumAddress(address)
    try {
        await stackeable_instance.methods.rewardsOf(address).call().then(function (rewards) {
            if (rewards) {
                console.log("Rewards: " + parseInt(rewards))
                return cb(parseInt(rewards))
            }
        })

    } catch (e) {
        throw new Error(e.message)
    } finally {
        return;

    }
}

/*
Function that verifies if an input is a valid ethereum address returning proper error message.

@params
address String

Example "0xDc29b9CE633F63394F1C5c4A6F9a6701F18fa9Ef"
*/

function validateEthereumAddress(address, cb) {
    var sufix = address.substring(0, 2);

    if (sufix != "0x") {
        let message = "Invalid ethereum address. Valid address should have '0x'"
        return cb(message);
    }
    else if (address.length < 42) {
        let message = "Invalid ethereum address length. Valid ethereum address should have 42 character length."
        return cb(message);
    }
    else {
        return cb(null)
    }

}

module.exports = { isStakeHolder, addStakeHolder, getStakingTokens, getRewards, validateEthereumAddress }

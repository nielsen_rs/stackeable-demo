var express = require('express')
var http = require('http')
var app = express()
var stackeable = require('./stackeable-controller')

app.get('/', (req, res) => {
  res.status(200).send("Welcome to the Stackeable Project API REST")
})

//Get 
app.get('/stakedTokens/:address', (req, res) => {
    let address = req.params.address
    stackeable.validateEthereumAddress(address, function(errMsg){
        
        if(errMsg){
            return res.status(401).json({"error":errMsg})
        }
        else{
            stackeable.getStakingTokens(address, function(total){
               res.status(200).json({"total_tokens":total})
            })
         }
    })
    
  })


app.get('/stakingRewards/:address', (req, res) => {
    let address = req.params.address
    stackeable.validateEthereumAddress(address, function(errMsg){

        if(errMsg){
            return res.status(401).json({"error":errMsg})
        }
        else{
            stackeable.getRewards(address, function(total){
                    return res.status(200).json({"total_rewards":total})
            })
         }
    })

})


app.get('/', (req, res) => {
    res.status(200).send("Welcome to the Stackeable Project API REST")
  })

http.createServer(app).listen(8002, () => {
  console.log('Server started at http://localhost:8002');
});

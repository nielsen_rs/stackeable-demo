const Migrations = artifacts.require("Migrations");
const stackeable = artifacts.require("Stackeable");
module.exports = function (deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(stackeable,'Stackeable Test','STKT');
};

This project has has been deployed with Truffle v5.1.30

Rest API has been developed with Node v14.8.0

To run API just run "node stackeable-api.js"

Default API port: 8002

Solidity version: 0.8.1



Smart Contract has been deployed on RSK private node. For instalation steps please refer to the following document.

Test had been made through regtest network

Docker:
https://developers.rsk.co/rsk/node/install/docker/

Ubuntu:
https://developers.rsk.co/rsk/node/install/ubuntu/


Added POSTMAN collection file: stackeable-demo.postman_collection


